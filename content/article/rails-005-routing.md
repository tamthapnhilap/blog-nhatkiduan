---
title: "Rails 005 Routing"
type: "article"
date: 2018-10-31T14:13:49+07:00
description: "Rails 005 Routing"
keywords: ["Rails 005 Routing"]
subjects: ["ror"]
image: "/common/no-image-available.jpg"
---

- [x] [Official](https://guides.rubyonrails.org/routing.html)
- [x] root to:'home#index' : home route
- [x] rake routes : list all routes
- [x] routing types: resources, namespace, nested resources, routing concerns
- [x] priority: top to bottom
- [x] Resources should never be nested more than 1 level deep