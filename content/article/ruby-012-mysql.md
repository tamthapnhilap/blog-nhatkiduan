---
title: "Ruby 012 Mysql"
type: "article"
date: 2018-09-12T10:58:43+07:00
description: "Ruby 012 Mysql"
keywords: ["Ruby 012 Mysql"]
subjects: ["ror"]
image: "/common/no-image-available.jpg"
---

## Temp

## Local run

```ruby 
bundle install --path=vendor
rails s -p 3000 -b 0.0.0.0 -e development
```

```bash
mkdir -p puma/pids puma/sockets puma/log
email = "adomik_test@adomik.com"
password = "test123456test"

CREATE USER 'webmaster'@'%' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON *.* TO 'webmaster'@'%' WITH GRANT OPTION;
SET PASSWORD FOR 'webmaster'@'%' = PASSWORD('123456');

mailcatcher --ip 0.0.0.0 -f -v
```