---
title: "Facebook Access Token"
type: "blog"
date: 2018-06-05T15:07:49+07:00
description: "Facebook Access Token"
keywords: ["Facebook Access Token"]
categories: ["facebook-api"]
tags: []
image: "/common/no-image-available.jpg"
---

## 1.Access Tokens

**Generating User Access Tokens**

There are two kinds of user access tokens:

- Short Term User Token Generation
- System User Token Generation

### 1.1. Short Term User Token Generation

